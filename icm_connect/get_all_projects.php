<?php

$response = array();

if (isset($_POST['user_id'])) {

$user_id = $_POST['user_id'];


require_once __DIR__ . '/db_connect.php';
$db = new DB_CONNECT();


$result = mysql_query("SELECT
	projects.id,
	project_user.user_id,
	projects.project_name,
	projects.project_client,
	projects.description,
	projects.note,
	projects.start_date,
	projects.end_date,
	projects.status,
	projects.folder,
	projects.updated_by,
	projects.deleted,
	projects.deleted_at,
	projects.deleted_by,
	projects.completed_on,
	projects.mark_completed_by,
	projects.created_at,
	projects.updated_at
	FROM projects, project_user
	WHERE projects.id = project_user.project_id
	AND project_user.user_id = $user_id") or die(mysql_error());


if (mysql_num_rows($result) > 0) {


    $response["projects"] = array();


    while ($row = mysql_fetch_array($result)) {


        $project = array();
        $project["id"] = $row["id"];
        $project["user_id"] = $row["user_id"];
        $project["project_name"] = $row["project_name"];
        $project["project_client"] = $row["project_client"];
        $project["description"] = $row["description"];
        $project["note"] = $row["note"];
        $project["start_date"] = $row["start_date"];
        $project["end_date"] = $row["end_date"];
        $project["status"] = $row["status"];
        $project["folder"] = $row["folder"];
        $project["updated_by"] = $row["updated_by"];
        $project["deleted"] = $row["deleted"];
        $project["deleted_at"] = $row["deleted_at"];
        $project["deleted_by"] = $row["deleted_by"];
        $project["completed_on"] = $row["completed_on"];
        $project["mark_completed_by"] = $row["mark_completed_by"];
        $project["created_at"] = $row["created_at"];
        $project["updated_at"] = $row["updated_at"];

 

        // push single task into final response array
        array_push($response["projects"], $project);

        }



    // success



    $response["success"] = 1;







    // echoing JSON response



    echo json_encode($response);



} else {



    // no tasks found



    $response["success"] = 0;



    $response["message"] = "No tasks found";

}
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echo no users JSON

    echo json_encode($response);

}




?>

