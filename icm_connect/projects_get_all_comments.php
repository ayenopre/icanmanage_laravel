<?php

// array for JSON response
$response = array();

require_once 'include/db_connect.php';
$db = new DB_CONNECT();

// check for post data
if (isset($_POST['project_id'])
	&& isset($_POST['user_id'])) {
		
    $project_id = $_POST['project_id'];
    $user_id = $_POST['user_id'];
	
	$tableName = 'projects_comments';

    $result = mysql_query("SELECT * FROM $tableName WHERE project_id = $project_id AND user_id = $user_id");

    if (!empty($result)) {
        // check for empty result
        if (mysql_num_rows($result) > 0) {

            $result = mysql_fetch_array($result);

            $data = array();
            $data["id"] = $result["id"];
            $data["project_id"] = $result["project_id"];
			$data["user_id"] = $result["user_id"];
			$data["comment"] = $result["comment"];
			$data["created_at"] = $result["created_at"];
			$data["updated_at"] = $result["updated_at"];
			
            // success
            $response["success"] = 1;

            // user node
            $response["data"] = array();

            array_push($response["data"], $data);

            // echoing JSON response
            echo json_encode($response);
        } else {
            // no data found
            $response["success"] = 0;
            $response["message"] = "No data found";

            // echo no users JSON
            echo json_encode($response);
        }
    } else {
        // no data found
        $response["success"] = 0;
        $response["message"] = "No data found";

        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>