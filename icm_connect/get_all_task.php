<?php



/*

 * Following code will list all the tasks

 */



// array for JSON response

$response = array();

if (isset($_POST['user_id'])) {
    
    $user_id = $_POST['user_id'];
    



// include db connect class

require_once __DIR__ . '/db_connect.php';



// connecting to db

$db = new DB_CONNECT();



// get all tasks from tasks table

$result = mysql_query("SELECT tasks.id, tasks.name, tasks.status, tasks.note, 

    tasks.folder, tasks.project_id, tasks.start_date, tasks.end_date,tasks.updated_by, 

    tasks.completed_on, tasks.deleted_at, tasks.created_at, tasks.updated_at, projects.project_name, project_user.user_id FROM tasks LEFT JOIN project_user

    ON tasks.project_id=project_user.project_id LEFT JOIN projects

    ON tasks.project_id=projects.id WHERE project_user.user_id = $user_id AND tasks.status != 'completed' AND tasks.deleted_at IS NULL") or die(mysql_error());





// check for empty result

if (mysql_num_rows($result) > 0) {

    // looping through all results

    // tasks node

    $response["tasks"] = array();

    

    while ($row = mysql_fetch_array($result)) {

        // temp user array

        

        $task = array();


        $task["id"] = $row["id"];


         
        $task["name"] = $row["name"];

        $task["status"] = $row["status"];

        $task["note"] = $row["note"];

        $task["folder"] = $row["folder"];

        $task["project_id"] = $row["project_id"];

        $task["start_date"] = $row["start_date"];

        $task["end_date"] = $row["end_date"];

        $task["updated_by"] = $row["updated_by"];

        $task["completed_on"] = $row["completed_on"];

        $task["deleted_at"] = $row["deleted_at"];

        $task["deleted_by"] = $row["deleted_by"];

        $task["created_at"] = $row["created_at"];

        $task["updated_at"] = $row["updated_at"];

        $task["project_name"] = $row["project_name"];

        $task["user_id"] = $row["user_id"];
      


        // push single task into final response array

        array_push($response["tasks"], $task);

    }

    // success

    $response["success"] = 1;



    // echoing JSON response

    echo json_encode($response);

} else {

    // no tasks found

    $response["success"] = 0;

    $response["message"] = "No tasks found";
}
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echo no users JSON

    echo json_encode($response);

}

?>

