<!DOCTYPE html>
<html>
<body>

<?php
echo "Today is " . date("Y/m/d") . "<br>";
echo "Today is " . date("Y.m.d") . "<br>";
echo "Today is " . date("Y-m-d") . "<br>";
echo "Today is " . date("d-m-Y") . "<br>";
echo "Today is " . date("l");
echo "Today is " . date("Y-m-d h:i:s");
?>

</body>
</html>