@extends('dashboard.default')
@section('head')
<title>ICM app - {{trans('92five.projectMonthlyReport')}}</title>
@stop
@section('content')
<div id="contentwrapper">
  <div class="main_content">
    <div class="row-fluid">
      <div class="span12 project_detail">
        <div class="monthly_title">Project {{$data['projectName']}} Gantt Chart</div>
      
        <div class="report-summay" style="height: 100px;">
          <h3>{{trans('92five.project')}}</h3>
             <div id="container"></div>
        </div>
       
      </div>
    </div>
  </div>
</div>
@stop
@section('endjs')
{{ HTML::script('assets/js/dashboard/moment.min.js') }}
{{ HTML::script('assets/js/dashboard/clndr.js') }}
{{ HTML::script('assets/js/dashboard/ChartNew.js') }}
{{ HTML::script('assets/js/dashboard/any-chart.js') }}
 <script type="text/javascript">
         anychart.onDocumentReady(function(){
  // create data tree on our data
     
        
  var treeData = anychart.data.tree({{$data_json}}, anychart.enums.TreeFillingMethod.AS_TABLE);
  // var treeData = anychart.data.tree(getData(), anychart.enums.TreeFillingMethod.AS_TABLE);

  
  // create project gantt chart
  chart = anychart.ganttProject();

  // set container id for the chart
  chart.container('container');

  // set data for the chart
  chart.data(treeData);

  // set start splitter position settings
  chart.splitterPosition(370);

  // get chart data grid link to set column settings
  var dataGrid = chart.dataGrid();


  // set first column settings
  var firstColumn = dataGrid.column(0);
  firstColumn.title('#');
  firstColumn.width(30);
  firstColumn.cellTextSettings().hAlign('center');

  // set second column settings

  var secondColumn = dataGrid.column(1);
  secondColumn.cellTextSettings().hAlign('left');
  secondColumn.width(180);

  // set third column settings
  var thirdColumn = dataGrid.column(2);
  thirdColumn.title('Start Date');
  thirdColumn.width(70);
  thirdColumn.cellTextSettings().hAlign('right');
  thirdColumn.textFormatter(function(item) {
    var date = new Date(item.get('start_date'));
    var month = date.getUTCMonth() + 1;
    var strMonth = (month > 9) ? month : '0' + month;
    var utcDate = date.getUTCDate();
    var strDate = (utcDate > 9) ? utcDate : '0' + utcDate;
    return date.getUTCFullYear() + '-' + strMonth + '-' + strDate;
  });

  // set fourth column settings
  var fourthColumn = dataGrid.column(3);
    fourthColumn.title('End Date');
    fourthColumn.width(80);
    fourthColumn.cellTextSettings().hAlign('right');
    fourthColumn.textFormatter(function(item) {
      var date = new Date(item.get('end_date'));
      var month = date.getUTCMonth() + 1;
      var strMonth = (month > 9) ? month : '0' + month;
      var utcDate = date.getUTCDate();
      var strDate = (utcDate > 9) ? utcDate : '0' + utcDate;
      return date.getUTCFullYear() + '-' + strMonth + '-' + strDate;
    });

  // initiate chart drawing
  chart.draw();

  // zoom chart to specified date
  // chart.zoomTo(Date.UTC(2016, 03, 05), Date.UTC(2016, 05, 24));
  cchart.zoomTo(Date.UTC(2010, 0, 8, 15), Date.UTC(2010, 3, 25, 20));;
});


function getData() {
  return [
    {"id": "1", "name": "Phase 1 - Strategic Plan",  "start_date":Date.UTC(2016, 04, 16), "end_date": Date.UTC(2016, 04, 30)},
     {"id": "1", "name": "Phase 1 - Strategic Plan",  "start_date":Date.UTC(2016, 04, 31), "end_date": Date.UTC(2016, 05, 07)},
      // {"id": "1", "name": "Phase 1 - Strategic Plan",  "start_date":Date.UTC(2016, 03-1, 18), "end_date": Date.UTC(2016, 12-1, 30)},
       // {"id": "1", "name": "Phase 1 - Strategic Plan",  "start_date":Date.UTC(2016, 03-1, 19), "end_date": Date.UTC(2016, 12-1, 30)},
    // {"id": "2", "name": "Define business vision", "progressValue": "15%", "start_date": 951350400000, "end_date": 954201600000},
    // {"id": "3", "name": "Identify available skills, information and support", parent:"2", "progressValue": "0%", "actualStart": 951494400000},
    // {"id": "4", "name": "Decide whether to proceed",  "progressValue": "0%", "actualStart": 951753600000, "actualEnd": 951786000000},
    // {"id": "5", "name": "Define the Opportunity",  "progressValue": "27%", "actualStart": 951782400000, "actualEnd": 952992000000},
    // {"id": "6", "name": "Research the market and competition",  "progressValue": "0%", "actualStart": 951840000000, "actualEnd": 951872400000},
    // {"id": "7", "name": "Interview owners of similar businesses",  "progressValue": "60%", "actualStart": 951868800000, "actualEnd": 952473600000},
    // {"id": "8", "name": "Identify needed resources",  "progressValue": "0%", "actualStart": 952531200000, "actualEnd": 952650000000},
    // {"id": "9", "name": "Identify operating cost elements", "progressValue": "0%", "actualStart": 952704000000, "actualEnd": 952995600000}
  
  ];
}
        </script>



<style>
         html, body, #container {
             width: 100%;
             height: 500%;
             margin: 0;
             padding: 0;
         }
        </style>



  @stop
